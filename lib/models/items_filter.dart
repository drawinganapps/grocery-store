class ItemsFilter {
  const ItemsFilter(this.name, this.initials);

  final String name;
  final String initials;
}
