import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/models/item_model.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;
  const ItemCardWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(5)),
                child: Image.asset('assets/img/${item.itemImage}'),
                height: 135),
            Positioned(
                top: 115,
                left: 130,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(color: Colors.green, width: 1.5),
                    color: Colors.white,
                  ),
                  child: const Icon(Icons.shopping_bag_outlined,
                      size: 15, color: Colors.black38),
                )),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(item.itemName,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                    color: Colors.green.shade600)),
            Container(
              margin: const EdgeInsets.only(top: 2),
            ),
            Text('\$' + item.itemPrice.toString() + '/per kg',
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 11,
                    color: Colors.grey)),
          ],
        )
      ],
    );
  }
}
