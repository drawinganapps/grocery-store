import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/controller/filter_controller.dart';
import 'package:food_app/models/items_filter.dart';
import 'package:get/get.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final ItemsFilter filter;
  const FilterCategoriesWidget({Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      return Center(
        child: Container(
          margin: const EdgeInsets.only(left: 15),
          height: 40,
          padding: const EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
              border: Border.all(
                  color: isSelected ? Colors.green : Colors.transparent,
                  width: 2),
              borderRadius: BorderRadius.circular(10)),
          child: Center(
            child: Text(filter.name,
                style: TextStyle(
                    color: isSelected ? Colors.green : Colors.grey,
                    fontSize: 18,
                    fontFamily: 'monospace',
                    fontWeight: FontWeight.bold)
            ),
          ),
        ),
      );
    });
  }
}
