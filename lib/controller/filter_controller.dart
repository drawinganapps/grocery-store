import 'package:get/get.dart';

class FilterController extends GetxController {
  var selectedFilter = 0;
  void changeFilter(int index)  {
    selectedFilter = index;
    update();
  }
}
