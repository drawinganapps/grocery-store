import 'package:food_app/models/item_model.dart';
import 'package:food_app/models/items_filter.dart';

class Dummy {
  static List<ItemModel> itemList = [
    ItemModel('mango.png', 'Mango', 6.4),
    ItemModel('tomatto.png', 'Tomato', 2.4),
    ItemModel('orange.png', 'Orange', 24.2),
    ItemModel('watermelon.png', 'Watermelon', 17.2),
    ItemModel('brocollies.png', 'Broccoli', 5.2),
    ItemModel('carrots.png', 'Carrot', 3.2),
  ];

  static List<ItemsFilter> filterList = <ItemsFilter>[
    const ItemsFilter('All Items', 'ALL'),
    const ItemsFilter('Vegetables', 'VEGETABLES'),
    const ItemsFilter('Fruits', 'FRUITS'),
    const ItemsFilter('Others', 'OTHERS'),
  ];
}
