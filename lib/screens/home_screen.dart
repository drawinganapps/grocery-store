import 'package:badges/badges.dart';
import 'package:blinking_text/blinking_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/controller/filter_controller.dart';
import 'package:food_app/data/Dummy.dart';
import 'package:food_app/models/item_model.dart';
import 'package:food_app/models/items_filter.dart';
import 'package:food_app/widgets/filter_widget.dart';
import 'package:food_app/widgets/item_card.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      List<ItemModel> items = Dummy.itemList;
      List<ItemsFilter> filters = Dummy.filterList;
      return Scaffold(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                    top: 20, left: 15, right: 15, bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(50)),
                        child: const Icon(Icons.dashboard)),
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(50)),
                      child: Badge(
                        position: const BadgePosition(top: 1, end: 1),
                        badgeColor: Colors.pinkAccent,
                        child: const Icon(Icons.notifications),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Row(
                  children: [
                    Flexible(
                        child: SizedBox(
                      height: 45,
                      child: TextField(
                        decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 15, right: 15),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide.none,
                              gapPadding: 0,
                            ),
                            fillColor: Colors.grey.withOpacity(0.2),
                            filled: true,
                            hintText: 'Search',
                            hintStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.italic,
                                color: Colors.black38)),
                      ),
                    )),
                    Container(
                      margin: const EdgeInsets.only(left: 10),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Icon(Icons.tune, color: Colors.grey.shade600),
                    ),
                  ],
                ),
              ),
              Container(
                  height: 50,
                  margin: const EdgeInsets.only(top: 20, bottom: 20),
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: List.generate(filters.length, (index) {
                        return GestureDetector(
                          child: FilterCategoriesWidget(
                              isSelected: index == controller.selectedFilter,
                              filter: filters[index]),
                          onTap: () {
                            controller.changeFilter(index);
                          },
                        );
                      }))),
              Container(
                width: 330,
                padding: const EdgeInsets.all(15),
                margin: const EdgeInsets.only(bottom: 20, left: 15),
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(5)),
                child: Row(
                  children: [
                    const Flexible(
                      child: BlinkText(
                        'Get your extra 15% off.',
                        duration: Duration(seconds: 5),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                            fontSize: 25,
                            fontFamily: 'Arial'),
                        beginColor: Colors.white,
                        endColor: Colors.green,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        )),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.pinkAccent),
                      ),
                      child: const Text('Claim',
                          style: TextStyle(
                              fontFamily: 'Arial',
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
              ),
              Expanded(
                child: GridView.count(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    crossAxisCount: 2,
                    crossAxisSpacing: 20,
                    childAspectRatio: 0.85,
                    children: List.generate(items.length, (index) {
                      return ItemCardWidget(item: items[index]);
                    })),
              )
            ],
          ),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
              color: Colors.grey.shade300,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.home_rounded,
                    color: Colors.green,
                    size: 30,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.chat_outlined,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
                Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(50)),
                  child: IconButton(
                    enableFeedback: false,
                    onPressed: () {},
                    icon: const Icon(
                      Icons.add_circle_outline,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: Badge(
                    position: BadgePosition.topEnd(top: -5, end: -6),
                    badgeColor: Colors.pinkAccent,
                    badgeContent: const Text(
                      '3',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.bold),
                    ),
                    child: const Icon(
                      Icons.shopping_bag_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                ),
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.person_outline_rounded,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
              ],
            ),
          ));
    });
  }
}
